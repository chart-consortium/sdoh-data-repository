# SDOH Data Repository

This repository hosts combined datasets of publicly available social determinants of health (SDOH) variables collected from various sources.

## Purpose

The purpose of this repository is threefold:

1. Provide a publicly-available combined dataset of SDOH variables
2. Provide source code showing how data were joined
3. Host raw source data files

## The Data

The datasets are split between decennial censuses (2010 and 2020) because census boundaries differ between them. Within a decennial census interval, the datasets are also split by geographic scale into the following:

1. States
2. Counties
3. Census tracts
4. Census block groups

Data dictionaries are also available for each file.

### List of variables

#### Variables Available

The overall list of variables and sources. For more details, please see the SDOH-metadata.csv file. 

| Variable           | Source             |
|:-------------------|:-------------------|
|State minimum wage  |US Department of Labor|
|Union Membership    |US Bureau of Labor Statistics|
|Area Health Resources|US Health Resources and Services Administration, Area Health Resources Files|
|Juvenile court cases|National Juvenile Court Data Archives|
|Food swamp index    |Food Environment Atlas|
|Social vulnerability index|US Centers for Disease Control and Prevention|
|Childen in single-parent households|American Community Survey (5-year)|
|Income inequality   |American Community Survey (5-year)|
|Children eligible for reduced priced lunch|American Community Survey (5-year)|
|Severe housing costs|American Community Survey (5-year)|
|Commute time        |American Community Survey (5-year)|
|Homeownership       |American Community Survey (5-year)|
|Health literacy     |UNC Health Literacy Map|
|Area Deprivation Index|UW-Madison Neighborhood Atlas|
|Environmental justice|US Environmental Protection Agency, Environmental Justice Screening and Mapping Tool|
|Uninsured           |American Community Survey (5-year)|
|Education attainment|American Community Survey (5-year)|
|Residential concentration of income|American Community Survey (5-year)|
|Broadband/Internet access|American Community Survey (5-year)|

#### Variables and Components

| Variable          | Variable Component |
|:------------------|:-------------------|
|State minimum wage |Minimum wage in state|
|Union Membership    |Percentage of total employed who have union membership|
|Union Membership    |Percentage of total employed who are represented by a union|
|Area Health Resources|Phys,Primary Care, Patient Care Non-Fed 2018|
|Area Health Resources|Psychiatry, Total Patient Care Non-Fed 2018|
|Area Health Resources|Dentists w/NPI 2018|
|Area Health Resources|Hosp W/Social Work Services Short Term General Hospitals 2018|
|Area Health Resources|Community Mental Health Ctrs 2018|
|Juvenile court cases|Total county population (juvenile court records)|
|Juvenile court cases|County population from age 10 through upper age (juvenile court records)|
|Juvenile court cases|County population from age 0 through upper age (juvenile court records)|
|Juvenile court cases|Number of juvenile court cases with delinquency petition status|
|Juvenile court cases|Number of juvenile court cases with delinquency non-petition status|
|Juvenile court cases|Number of juvenile court cases with status non-petition status|
|Juvenile court cases|Number of juvenile court cases with status non-petition status|
|Food swamp index    |Food swamp index|
|Social vulnerability index|SVI|
|Childen in single-parent households|Population under 18 years in households, total|
|Childen in single-parent households|Population under 18 years in households, in male householder, no spouse/partner present household|
|Childen in single-parent households|Population under 18 years in households, In female householder, no spouse/partner present household|
|Income inequality   |Gini index of income inequality in all households|
|Children eligible for reduced priced lunch|Population under 18 years in households, total|
|Children eligible for reduced priced lunch|Living in household with supplemental Security Income (SSI), cash public assistance income, or Food Stamps/SNAP in past 12 months, under 18 years in household|
|Severe housing costs|Housing costs as percentage of household income in past 12 months, total occupied housing units|
|Severe housing costs|Housing costs as percentage of household income in past 12 months, owner-occupied, income less than $20,000, 30 percent or more|
|Severe housing costs|Housing costs as percentage of household income in past 12 months, owner-occupied, income $20,000 to $34,999, 30 percent or more|
|Severe housing costs|Housing costs as percentage of household income in past 12 months, owner-occupied, income $35,000 to $49,000, 30 percent or more|
|Severe housing costs|Housing costs as percentage of household income in past 12 months, owner-occupied, income $50,000 to $74,000, 30 percent or more|
|Severe housing costs|Housing costs as percentage of household income in past 12 months, owner-occupied, income 75,000 or more, 30 percent or more|
|Severe housing costs|Housing costs as percentage of household income in past 12 months, renter-occupied, income less than $20,000, 30 percent or more|
|Severe housing costs|Housing costs as percentage of household income in past 12 months, renter-occupied, income $20,000 to $34,999, 30 percent or more|
|Severe housing costs|Housing costs as percentage of household income in past 12 months, renter-occupied, income $35,000 to $49,000, 30 percent or more|
|Severe housing costs|Housing costs as percentage of household income in past 12 months, renter-occupied, income $50,000 to $74,000, 30 percent or more|
|Severe housing costs|Housing costs as percentage of household income in past 12 months, renter-occupied, income 75,000 or more, 30 percent or more|
|Commute time        |Travel time to work for workers 16 years and over who did not work from home, less than 10 min|
|Commute time        |Travel time to work for workers 16 years and over who did not work from home, 10 to 14 min|
|Commute time        |Travel time to work for workers 16 years and over who did not work from home, 15 to 19 min|
|Commute time        |Travel time to work for workers 16 years and over who did not work from home, 20 to 24 min|
|Commute time        |Travel time to work for workers 16 years and over who did not work from home, 25 to 29 min|
|Commute time        |Travel time to work for workers 16 years and over who did not work from home, 30 to 34 min|
|Commute timee        |Travel time to work for workers 16 years and over who did not work from home, 35 to 44 min|
|Commute time        |Travel time to work for workers 16 years and over who did not work from home, 45 to 59 min|
|Commute time       |Travel time to work for workers 16 years and over who did not work from home, 60 or more min|
|Homeownership       |Homeownership, percentage of owner-occupied housing units|
|Health literacy     |Health literacy estimate|
|Area Deprivation Index|National ADI percentile (2015)|
|Area Deprivation Index|State ADI percentile (2015)|
|Environmental justice|EJ Index for % pre-1960 housing (lead paint indicator)|
|Environmental justice|EJ Index for Diesel particulate matter level in air|
|Environmental justice|EJ Index for Air toxics cancer risk|
|Environmental justice|EJ Index for Air toxics respiratory hazard index|
|Environmental justice|EJ Index for Traffic proximity and volume|
|Environmental justice|EJ Index for Indicator for major direct dischargers to water|
|Environmental justice|EJ Index for Proximity to National Priorities List (NPL) sites|
|Environmental justice|EJ Index for Proximity to Risk Management Plan (RMP) facilities|
|Environmental justice|EJ Index for Proximity to Treatment Storage and Disposal (TSDF) facilities|
|Environmental justice|EJ Index for Ozone level in air|
|Environmental justice|EJ Index for PM2.5 level in air|
|Uninsured           |Health insurance, total civilian non-institutionalized population|
|Uninsured           |Health insurance, no health insurance coverage, under 19 years|
|Uninsured           |Health insurance, no health insurance coverage, 19 to 34 years|
|Uninsured           |Health insurance, no health insurance coverage, 35 to 64 years|
|Uninsured           |Health insurance, no health insurance coverage, 65 years and older|
|Education attainment|Education attainment, total population 25 years and over|
|Education attainment|Education attainment, high school, 25 years and over, male|
|Education attainment|Education attainment, high school, 25 years and over, female|
|Education attainment|Education attainment, bachelor's degree, 25 years and over, male|
|Education attainment|Education attainment, bachelor's degree, 25 years and over, female|
|Residential concentration of income|Household income, total households|
|Residential concentration of income|Household income, households less than $10,000|
|Residential concentration of income|Household income, households $10,000 to $14,999|
|Residential concentration of income|Household income, households $15,000 to $19,999|
|Residential concentration of income|Household income, households $125,000 to $149,999|
|Residential concentration of income|Household income, households $150,000 to $199,999|
|Residential concentration of income|Household income, households $200,000 or more|
|Broadband/Internet access|Internet subscriptions in household, total households|
|Broadband/Internet access|Internet subscriptions in household, households with broadband such as cable, fiber optic, or DSL|

#### Notes 

- **Percentage of total employed who have union membership:** Data refer to members of a labor union or an employee association similar to a union. NOTE: Data refer to the sole or principal job of full- and part-time wage and salary workers. All self-employed workers are excluded, both those with incorporated businesses as well as those with unincorporated businesses.
- **Percentage of total employed who are represented by a union:** Data refer to both union members and workers who report no union affiliation but whose jobs are covered by a union or an employee association contract. NOTE: Data refer to the sole or principal job of full- and part-time wage and salary workers. All self-employed workers are excluded, both those with incorporated businesses as well as those with unincorporated businesses.

### How to Use the Data

These data are intended to be joined with patient records, and to do so requires a geocoded patient address and corresponding FIPS/GEOIDs for each spatial scale. This can be done using geocoding tools (Melissa, DeGAUSS, etc.). 

#### Example Workflow

1. Geocode each patient's address and obtain geolocation and corresponding FIPS codes for census block group or block. 
2. Derive FIPS/GEOIDs for all other spatial scales listed above using block group GEOIDs ([explanation of nested structure of GEOIDs](https://www.census.gov/programs-surveys/geography/guidance/geo-identifiers.html))
3. Load SDOH datasets provided in the repo (see below for how to get the data)
4. Iteratively outer join patient records with each SDOH dataset using the appropriate FIPS (block group, tract, etc.)
5. Proceed with data cleaning and analyses


### How to Get the Data

**Data Files**

Data files for 2010 and 2020 census boundaries can be downloaded using the links below. Clicking a link will initiate a download of a ZIP file that contains the 4 joined data files (one for each spatial scale listed above) in CSV format.

- [2010 Census Boundaries](https://chart-consortium.pages.oit.duke.edu/sdoh-data-repository/joined.zip)
- 2020 Census Boundaries (not yet available)

**Data Dictionaries**

Data dictionaries for each file can be downloaded for 2010 and 2020 census boundaries can be downloaded using the links below. 

- [2010 Census Boundaries Data Dictionaries](https://chart-consortium.pages.oit.duke.edu/sdoh-data-repository/dicts.zip)
- 2020 Census Boundaries Data Dictionaries (not yet available)

## Methodologies

More detailed explanations and notes regarding methodology can be found in separate files for each dataset:

1. [2010 Census Boundaries](https://gitlab.oit.duke.edu/chart-consortium/sdoh-data-repository/-/blob/main/2010_boundaries/data-notes.md)
2. 2020 Census Boundaries (not yet available)

### Reproducibility

For reproducibility, the R code to generate the datasets runs via a GitLab CI/CD pipeline using a Docker container with current verisons of common open-source R tools, which itself is based on the [Rocker geospatial container](https://rocker-project.org/images/versioned/rstudio.html):

- The container repo is [here](https://gitlab.oit.duke.edu/chart-consortium/containers/geospatial-docker)
- The container recipe file is [here](https://gitlab.oit.duke.edu/chart-consortium/containers/geospatial-docker/-/blob/main/Dockerfile)
- The container can be pulled from Duke's public server with the following Docker command:

```
docker pull gitlab-registry.oit.duke.edu/chart-consortium/containers/geospatial-docker:latest
```
