# 2010 Boundary Data Notes

Notes and questions related to acquiring data from online sources.

## Notes

### General

- This dataset is specific to 2010 census boundaries with the newest available year of 2019
- Compiled datasets are available in /2010_boundaries/joined
- Data dictionaries are available in /2010_boundaries/data_dictionaries
- Only raw variables are included with the exception of the PhenX variable food swamp (RFEI, see note below)
- Raw data was manipulated as little as possible within reason
  + Missing values of "-999"" and "--" were replaced with R standard value NA
  + Missing values, non-standard or otherwise, were not otherwise modified or imputed
  + Column names were kept as found in the raw data files whenever feasible
- Variables were left-joined to a standard list of FIPS codes for each spatial scale such that missing values were created for FIPS not represented by a given variable. The standard FIPS codes for the 50 states + DC were as follows:
  + Block group:  2019 TIGER/LINE block group shape files, acquired using R package 'tigris' (2.0)
  + Census tracts: 2019 TIGER/LINE census tract shape files, acquired using R package 'tigris' (2.0)
  + Counties: Current county-level FIPS codes from built-in 'fips_codes' dataset in R package 'tigris' (2.0)
  + States: Current state-level FIPS codes from built-in 'fips_codes' dataset in R package 'tigris' (2.0)
- ACS variables of interest were determined by looking at the [table shells for each ACS table](https://www2.census.gov/programs-surveys/acs/tech_docs/table_shells/2019/) and these were put into the SDOH-metadata.csv


### Specific Variables

1. Juvenile court cases
  - A special value of "*" represents a non-missing value for number of court cases between 1 - 4
  - Total cases = Delinquency_Petition + Delinquency_Non_Petition + Status_Petition + Status_Non_Petition
  - Note there are many counties with missing values
    + AK: Several counties with no court case data
    + CT: Court case data not listed by county
    + FL: Dade county was rename to Miami+Dade in 1997; Dade is dropped because it's not present in the juvenile court cases data
    + KY: No court case data available
    + LA: No court case data available
    + MT: Yellowstone National Park has no court case data available
    + NH: No court case data available
    + SD: Shannon County missing from court case data
    + VA: Bedford City, Clifton Forge City, and South Boston City missing from court case data. Note that Bedford county is present. 
2. Food swamp
    - From PhenX:  The presence of a food swamp is calculated using the traditional Retail Food Environment Index per county, which includes the number of fast food restaurants and convenience stores divided by the number of grocery stores and supermarkets. The number of grocery stores, fast food restaurants, and convenience stores is determined at a county level using data from the **Food Environment Atlas**.
    - Traditional Retail Food Environment Index (RFEI) = (Fast Food / Limited Service Establishments + Convenience Stores) / (Grocery Stores / Super Markets)
        + RFEI = (FFR14 + CONVS14) / (GROC14 + SUPERC14)
    - Several observations with a value of +Inf
    - Several observations with missing values
3. Percent unionized for non-agricultural workforce
  - Each variable has a different "series ID" -- the last 2 digits are the state FIPS code. 
    + Prefix for percent of employed represented by union: LUU02048997
    + Prefix for percent of employed member of union: LUU02048996
4. Social Vulnerability Index (SVI)
  - The column "RPL_THEMES" from the raw data file was used, which contains the nationally ranked values as opposed to state ranks

